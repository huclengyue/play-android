package com.apkdv.buildsrc

import org.gradle.api.artifacts.dsl.DependencyHandler

object Versions {
    const val ktlint = "0.39.0"

    // android
    const val compileVersion = 31
    const val minVersion = 24
    const val buildVersion = 31
    const val targetVersion = 31
    const val versionName = "1.0.0"
    const val versionCode = 1
    // kotlin

    const val kotlin = "1.5.31"
    const val composeKotlin = "1.5.31"


    // Plugin
    const val gradle = "7.0.3"
    const val compose = "1.0.5"
    const val androidx = "1.3.1"
    const val constraint = "2.1.0"
    const val annotation = "1.2.0"
    const val lifecycle = "2.2.0"
    const val coil = "1.4.0"
    const val rxhttp = "2.6.8"
}

object AndroidClassPath {
    const val gradlePlugin = "com.android.tools.build:gradle:${Versions.gradle}"
    const val kotlinPlugin = "com.android.tools.build:gradle:${Versions.kotlin}"
}

object AndroidX {
    const val material = "com.google.android.material:material:1.4.0"
    const val appcompat = "androidx.appcompat:appcompat:${Versions.androidx}"
    const val coreKtx = "androidx.core:core-ktx:1.3.2"
    const val constraint = "androidx.constraintlayout:constraintlayout:${Versions.constraint}"

    const val annotation = "androidx.annotation:annotation:${Versions.annotation}"
    internal val androidNeeds = listOf(
        AndroidX.material, AndroidX.appcompat,
        AndroidX.coreKtx, AndroidX.constraint, AndroidX.annotation
    )


}

fun DependencyHandler.addAndroidx() {
    AndroidX.androidNeeds.forEach {
        add("api", it)
    }
}

fun DependencyHandler.addCompose() {
    Compose.compose.forEach {
        add("api", it)
    }
}

fun DependencyHandler.addNetWork() {
    NetWork.net.forEach {
        add("api", it)
    }
}

fun DependencyHandler.addLifecycle() {
    Lifecycle.lifecycle.forEach {
        add("api", it)
    }
}

fun DependencyHandler.addPaging() {
    Paging.paging.forEach {
        add("api", it)
    }
}

object Compose {
    val composeUi = "androidx.compose.ui:ui:${Versions.compose}"
    val composeTooling = "androidx.compose.ui:ui-tooling:${Versions.compose}"
    val foundation = "androidx.compose.foundation:foundation:${Versions.compose}"
    val foundationLayout = "androidx.compose.foundation:foundation-layout:${Versions.compose}"
    val composeMaterial = "androidx.compose.material:material:${Versions.compose}"
    val composePreview = "androidx.compose.ui:ui-tooling-preview:${Versions.compose}"

    //viewModel and LiveData
    val composeLivedata = "androidx.compose.runtime:runtime-livedata:${Versions.compose}"
    val composeActivity = "androidx.activity:activity-compose:1.3.1"
    val systemUiController = "com.google.accompanist:accompanist-systemuicontroller:0.14.0"
    val insets = "com.google.accompanist:accompanist-insets:0.14.0"
    const val constraintCompose =
        "androidx.constraintlayout:constraintlayout-compose:1.0.0-alpha01"
    const val composeNavigation = "androidx.navigation:navigation-compose:2.4.0-alpha10"

    internal val compose = listOf(
        composeUi, composeTooling,
        foundation, composeMaterial,
        composePreview, constraintCompose, foundationLayout,
        composeLivedata, composeActivity, composeNavigation,
        systemUiController, insets
    )
}

object Paging {
    val pagingRuntime = "androidx.paging:paging-runtime-ktx:3.0.1"
    val composePaging = "androidx.paging:paging-compose:1.0.0-alpha14"

    internal val paging = listOf(
        pagingRuntime, composePaging
    )
}


object NetWork {
    val okhttp = "com.squareup.okhttp3:okhttp:4.9.1"
    val rxhttp = "com.github.liujingxing.rxhttp:rxhttp:${Versions.rxhttp}"
    val rxhttpCompiler = "com.github.liujingxing.rxhttp:rxhttp-compiler:${Versions.rxhttp}"
    internal val net = listOf(NetWork.okhttp, NetWork.rxhttp)

}


object Lifecycle {
    val viewmodel = "androidx.lifecycle:lifecycle-viewmodel-compose:1.0.0-alpha07"
    val runtime = "androidx.lifecycle:lifecycle-runtime-ktx:${Versions.lifecycle}"
    const val viewmodelKtx = "androidx.lifecycle:lifecycle-viewmodel-ktx:${Versions.lifecycle}"

    internal val lifecycle = listOf(Lifecycle.viewmodel, Lifecycle.viewmodelKtx, Lifecycle.runtime)

}


object Deps {

    val gson = "com.google.code.gson:gson:2.8.5"

    // arouter
    val arouter = "com.alibaba:arouter-api:1.5.1"
    val arouterCompiler = "com.alibaba:arouter-compiler:1.5.1"

    // MMKV
    val mmkv = "com.tencent:mmkv-static:1.2.11"

    // 图片加载
    val coil = "io.coil-kt:coil:${Versions.coil}"
    val coilGif = "io.coil-kt:coil-gif:${Versions.coil}"
    val coilCompose = "io.coil-kt:coil-compose:${Versions.coil}"

    // utils
    val utilcodex = "com.blankj:utilcodex:1.30.6"

    // 图片选择
    val pictureselector = "com.github.LuckSiege.PictureSelector:picture_library:v2.6.1"

    //material-dialogs
    val dialogx = "com.github.kongzue.dialogx:DialogX:0.0.37"

    //coroutines
    val coroutines = "org.jetbrains.kotlinx:kotlinx-coroutines-android:1.5.2"

}
