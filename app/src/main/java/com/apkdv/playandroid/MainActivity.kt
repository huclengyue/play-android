package com.apkdv.playandroid

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.core.view.WindowCompat
import coil.annotation.ExperimentalCoilApi
import com.apkdv.mvvmfast.network.RxHttpManager
import com.apkdv.playandroid.compose.MainCompose
import com.apkdv.playandroid.ui.theme.PlayAndroidTheme
import com.google.accompanist.insets.ProvideWindowInsets

@ExperimentalCoilApi
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //设置为沉浸式状态栏
        RxHttpManager(application)
        //设置为沉浸式状态栏
        WindowCompat.setDecorFitsSystemWindows(window, false)
//        WindowCompat.setDecorFitsSystemWindows(window, true)
        setContent {
            PlayAndroidTheme {
                //可以获取状态栏高度
                ProvideWindowInsets {
                    MainCompose {
                        finish()
                    }
                }
            }
        }
    }
}
