package com.apkdv.playandroid.api

import com.apkdv.mvvmfast.network.Http
import com.apkdv.playandroid.entity.PublicHistoryData
import com.apkdv.playandroid.entity.PublicNumberListItem
import com.apkdv.playandroid.paging.CommonalityPageModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Deferred
import rxhttp.async
import rxhttp.wrapper.param.toResponse

object API {
    val PAGE_SIZE = 50

    /**
     * 获取公众号列表
     */
    suspend fun getPublicNumberList(): ArrayList<PublicNumberListItem> {
        return Http.get("wxarticle/chapters/json")
            .toResponse<ArrayList<PublicNumberListItem>>().await()
    }

    /**
     * 查看某个公众号历史数据
     * https://wanandroid.com/wxarticle/list/408/1/json
     */
    suspend fun getPublicHistoryData(id: Int, page: Int): PublicHistoryData {
        return Http.get("wxarticle/list/$id/$page/json?page_size=$PAGE_SIZE")
            .toResponse<PublicHistoryData>().await()
    }


    /**
     * 在某个公众号中搜索历史文章
     * https://wanandroid.com/wxarticle/list/405/1/json?k=Java
     */
    suspend fun getSearchDataAsync(
        scope: CoroutineScope,
        id: Int,
        page: Int,
        key: String,
    ): Deferred<PublicHistoryData> {
        return Http.get("wxarticle/list/$id/$page/json?page_size=$PAGE_SIZE&k=$key")
            .toResponse<PublicHistoryData>().async(scope)
    }
}