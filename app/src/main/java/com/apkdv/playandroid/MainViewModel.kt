package com.apkdv.playandroid

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import com.apkdv.mvvmfast.base.BaseViewModel
import com.apkdv.mvvmfast.entity.*
import com.apkdv.mvvmfast.ext.launchWithFlow
import com.apkdv.mvvmfast.ext.netCatch
import com.apkdv.mvvmfast.ext.next
import com.apkdv.mvvmfast.ext.showToast
import com.apkdv.playandroid.api.API
import kotlinx.coroutines.flow.onStart

class MainViewModel : BaseViewModel() {
    var listData by mutableStateOf<LoadState>(LoadNano)

    fun getList() {
        request {
            launchWithFlow({
                API.getPublicNumberList()
            }, {
                it.message.showToast()
            }).onStart {
                listData = LoadLoading
            }.netCatch {
                listData = LoadError(this)
            }.next {
                listData = LoadSuccess(this)
            }
        }
    }

    init {
        getList()
    }

}