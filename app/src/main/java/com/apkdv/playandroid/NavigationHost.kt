package com.apkdv.playandroid

import androidx.activity.compose.BackHandler
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.navArgument
import coil.annotation.ExperimentalCoilApi
import com.apkdv.mvvmfast.compose.statelayout.StateLayout
import com.apkdv.playandroid.compose.PageTabRow
import com.apkdv.playandroid.compose.SquareCompose
import com.apkdv.playandroid.compose.WebViewCompose
import com.apkdv.playandroid.entity.PublicNumberListItem
import com.apkdv.playandroid.widget.Nav
import com.apkdv.playandroid.widget.StatsBarUtil
import com.google.accompanist.insets.statusBarsHeight

/**
 * 内容 导航
 */
@ExperimentalCoilApi
@Composable
fun NavigationHost(
    squareViewModel: SquareViewModel, onFinish: () -> Unit,
    navController: NavHostController
) {

    NavHost(
        navController,
        startDestination = "main"
    ) {
        //主页面
        composable(
            route = "main"
        ) {
            //系统颜色的状态栏
//            StatsBarUtil().StatsBarColor(false)
            StateLayout<ArrayList<PublicNumberListItem>>(
                state = squareViewModel.navData,
                onReload = {
                    squareViewModel.getNavi()
                }) {
                Column {
                    //内容不挡住状态栏 如果不设置颜色这里会自己适配，但可能产生闪烁
                    Spacer(
                        modifier = Modifier
                            .background(MaterialTheme.colors.primary)
                            .statusBarsHeight()
                            .fillMaxWidth()
                    )
                    PageTabRow(it, Nav.squareTopBarIndex)
                    SquareCompose(it, navController)
                }

            }


            //点击两次返回才关闭app
//            BackHandler {
//                TwoBackFinish().execute(context, onFinish)
//            }
        }

        //H5页面
        composable(
            route = "webview?url={url}", arguments = listOf(
                navArgument("url"){
                    defaultValue = "https://www.wanandroid.com/"
                })
        ) { backStackEntry ->

            //系统颜色的状态栏
            StatsBarUtil().StatsBarColor(false)

            WebViewCompose(
                navController,
                backStackEntry.arguments?.getString("url") ?: "https://www.wanandroid.com"
            )

            BackHandler { navController.navigateUp() }
        }
    }
}

/**
 * 页面跳转关键类
 */
enum class KeyNavigationRoute(
    val route: String
) {
    //主页面
    MAIN("main"),

    //H5
    WEBVIEW("webview"),

}










