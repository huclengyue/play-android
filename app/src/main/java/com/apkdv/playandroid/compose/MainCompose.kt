package com.apkdv.playandroid.compose

import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.runtime.Composable
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import coil.annotation.ExperimentalCoilApi
import com.apkdv.playandroid.NavigationHost
import com.apkdv.playandroid.SquareViewModel


/**
 * 主界面
 * [onFinish] 点击两次返回关闭页面
 */
@ExperimentalCoilApi
@Composable
fun MainCompose(onFinish: () -> Unit) {
    val squareViewModel: SquareViewModel = viewModel()
    Content(squareViewModel, onFinish)
}

@OptIn(ExperimentalCoilApi::class)
@Composable
private fun Content(
    squareViewModel: SquareViewModel,
    onFinish: () -> Unit,
    navHostController: NavHostController = rememberNavController(),
) {
    Scaffold(
        contentColor = MaterialTheme.colors.background,
        //内容
        content = { paddingValues: PaddingValues ->

            NavigationHost(squareViewModel, onFinish, navHostController)

        }
    )
}

