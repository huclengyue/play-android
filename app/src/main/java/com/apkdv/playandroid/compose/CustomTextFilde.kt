package com.apkdv.playandroid.compose

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.SolidColor
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.apkdv.mvvmfast.ext.isNotNullEmpty
import com.apkdv.playandroid.R
import com.blankj.utilcode.util.Utils
import com.jmbon.group.ket.colorResource

@Composable
fun CustomTextField(
    modifier: Modifier = Modifier,
    hint: String? = null,
    showCleanIcon: Boolean = false,
    onTextChange: String.() -> Unit = {},
    leadingIcon: @Composable (() -> Unit)? = null,
    trailingIcon: @Composable (() -> Unit)? = null,
    keyboardOptions: KeyboardOptions = KeyboardOptions.Default,
    keyboardActions: String.() -> Unit = {},
    textFieldStyle: TextStyle = defaultTextStyle,
    hintTextStyle: TextStyle = defaultHintTextStyle,

    ) {
    var text by remember { mutableStateOf("") }
    Row(
        modifier,
        verticalAlignment = Alignment.CenterVertically,
    ) {
        leadingIcon?.invoke()
        BasicTextField(
            value = text,
            onValueChange = {
                text = it
                onTextChange.invoke(it)
            },
            cursorBrush = SolidColor(colorResource(id = R.color.color_currency)),
            singleLine = true,
            modifier = Modifier
                .weight(1f)
                .padding(start = 10.dp),
            textStyle = textFieldStyle,
            decorationBox = { innerTextField ->
                if (text.isBlank() && hint.isNotNullEmpty())
                    Box(
                        modifier = Modifier
                            .fillMaxHeight(),
                        contentAlignment = Alignment.CenterStart
                    ) {
                        innerTextField()
                        CustomText(hint ?: "", 16f.sp, colorResource(id = R.color.color_BFBFBF))
                        Text(
                            hint ?: "",
                            modifier = Modifier
                                .fillMaxWidth(),
                            style = hintTextStyle,
                        )
                    } else innerTextField()

            },
            keyboardActions = KeyboardActions {
                keyboardActions(text)
            },
            keyboardOptions = keyboardOptions
        )
        trailingIcon?.invoke()
        if (showCleanIcon)
            ImageIcon(R.drawable.icon_edit_clean, 24.dp) {
                text = ""
            }
    }
}

@Composable
fun CustomText(value: String, textSize: TextUnit, textColor: Color) {
    Text(
        value,
        modifier = Modifier
            .fillMaxWidth(),
        style = TextStyle(
            color = textColor,
            fontSize = textSize,
        ),
    )
}

val defaultHintTextStyle = TextStyle(
    color = Utils.getApp().colorResource(R.color.color_BFBFBF),
    fontSize = 16.sp,
)
val defaultTextStyle = TextStyle(
    color = Utils.getApp().colorResource(R.color.color_262626),
    fontSize = 16.sp,
)


