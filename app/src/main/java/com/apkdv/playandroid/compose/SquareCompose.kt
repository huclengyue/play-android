package com.apkdv.playandroid.compose

import android.util.Log
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import androidx.viewpager2.widget.ViewPager2
import com.apkdv.mvvmfast.compose.statelayout.StateLayout
import com.apkdv.playandroid.KeyNavigationRoute
import com.apkdv.playandroid.SquareViewModel
import com.apkdv.playandroid.entity.PublicHistoryData
import com.apkdv.playandroid.entity.PublicNumberListItem
import com.apkdv.playandroid.widget.Nav

/**
 * 广场页面
 */
@Composable
fun SquareCompose(userList: ArrayList<PublicNumberListItem>, navHostController: NavHostController) {
    val squareViewModel: SquareViewModel = viewModel()
    ViewPager(
        Modifier
            .fillMaxSize()
    ) {
        //Adding 13th child(screen) to viewpager without looping.
        userList.forEach {
            ViewPagerChild {
                Box(
                    Modifier.fillMaxSize(),
                    contentAlignment = Alignment.Center
                ) {
                    StateLayout<PublicHistoryData>(state = squareViewModel.userData, onReload = {
                        squareViewModel.getDataById(it.id)
                    }) {
                        SquareAndQuestionComposable(
                            //        navHostController,
                            it.datas.toMutableList(),navHostController = navHostController
                        )
                    }

                }
            }
        }


        // ---------------- OPTIONAL THINGS ----------------
        //You can use a ViewPager2 object to be able to receive callbacks by adding some listeners
        //or can attach it with some TabLayout like things.
        //For very own custom ViewPager in Composable scope user can override its adapter and other
        //properties as well.
        androidViewPager2.registerOnPageChangeCallback(object :
            ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                if (position != Nav.squareTopBarIndex.value) {
                    Nav.squareTopBarIndex.value = position
                    squareViewModel.getDataById(userList[Nav.squareTopBarIndex.value].id)
                    Log.d(TAG, "onPageSelected() called with: position = $position")
                }
            }
        })
        androidViewPager2.currentItem =
            if (androidViewPager2.currentItem != Nav.squareTopBarIndex.value) {
                Log.d(
                    TAG,
                    "onPageSelected() called with: position = ${Nav.squareTopBarIndex.value}"
                )
                squareViewModel.getDataById(userList[Nav.squareTopBarIndex.value].id)
                Nav.squareTopBarIndex.value
            } else Nav.squareTopBarIndex.value
    }

}

private const val TAG = "SquareCompose"

val squareIndexState: LazyListState = LazyListState()

/**
 * 广场和问答页面
 */
@Composable
private fun SquareAndQuestionComposable(
    list: MutableList<PublicHistoryData.HistoryData>,
    state: LazyListState = rememberLazyListState(), navHostController: NavHostController
) {

    LazyColumn(modifier = Modifier.fillMaxSize(), state = state) {
        items(list) { item ->
            HomeCardItemContent(
                getAuthor(item.author, item.shareUser),
                item.fresh,
                false,
                item.niceDate ?: "刚刚",
                item.title ?: "",
                item.superChapterName ?: "未知",
                item.collect
            ) {
                navHostController.navigate("${KeyNavigationRoute.WEBVIEW.route}?url=${item.link}")

            }
        }
    }

}
