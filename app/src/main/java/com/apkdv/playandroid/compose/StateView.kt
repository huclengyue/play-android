package com.apkdv.playandroid.compose

import android.annotation.SuppressLint
import android.view.LayoutInflater
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.ui.Modifier
import androidx.compose.ui.viewinterop.AndroidView
import com.apkdv.mvvmfast.R
import androidx.compose.runtime.Composable
import androidx.compose.ui.tooling.preview.Preview
import com.apkdv.mvvmfast.compose.statelayout.EmptyPage


@SuppressLint("InflateParams")
@Composable
fun EmptyCompose() {
    EmptyPage()
}

@Preview
@Composable
fun EmptyComposePreview() {
    EmptyCompose()
}