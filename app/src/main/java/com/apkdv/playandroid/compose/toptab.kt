package com.apkdv.playandroid.compose

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.material.MaterialTheme
import androidx.compose.material.ScrollableTabRow
import androidx.compose.material.Tab
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.apkdv.playandroid.entity.PublicNumberListItem

@Composable
fun PageTabRow(
    items: ArrayList<PublicNumberListItem>,
    selectedIndex: MutableState<Int>
) {
    ScrollableTabRow(
        selectedTabIndex = selectedIndex.value,
        modifier = Modifier
            .fillMaxWidth(),
        backgroundColor = MaterialTheme.colors.primary
    ) {
        items.forEachIndexed { index, item ->
            Tab(
                text = { Text(item.name) },
                selected = index == selectedIndex.value,
                onClick = {
                    selectedIndex.value = index
                }
            )
        }
    }
}