package com.apkdv.playandroid.compose

import androidx.annotation.DrawableRes
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.size
import androidx.compose.material.IconButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.Dp

@Composable
fun ImageIcon(@DrawableRes drawable: Int, size: Dp, clickable: (() -> Unit)? = null) {
    if (clickable == null) {
        DrawableImage(drawable, size)
    } else {
        IconButton(onClick = clickable) {
            DrawableImage(drawable, size)
        }
    }
}

@Composable
fun DrawableImage(@DrawableRes drawable: Int, size: Dp) {
    Image(
        painterResource(drawable),
        contentDescription = "",
        contentScale = ContentScale.Crop,
        modifier = Modifier
            .size(size)
    )
}
