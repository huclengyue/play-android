//package com.apkdv.playandroid.compose
//
//import android.app.Activity
//import androidx.compose.foundation.background
//import androidx.compose.foundation.layout.Row
//import androidx.compose.foundation.layout.fillMaxWidth
//import androidx.compose.foundation.layout.height
//import androidx.compose.foundation.layout.padding
//import androidx.compose.foundation.shape.RoundedCornerShape
//import androidx.compose.foundation.text.KeyboardOptions
//import androidx.compose.material.Icon
//import androidx.compose.material.IconButton
//import androidx.compose.material.icons.Icons
//import androidx.compose.material.icons.outlined.ArrowBack
//import androidx.compose.runtime.Composable
//import androidx.compose.ui.Alignment
//import androidx.compose.ui.Modifier
//import androidx.compose.ui.graphics.Color
//import androidx.compose.ui.res.colorResource
//import androidx.compose.ui.text.input.ImeAction
//import androidx.compose.ui.unit.dp
//import com.apkdv.playandroid.compose.CustomTextField
//import com.apkdv.playandroid.compose.ImageIcon
//
//@Composable
//fun SearchTopBar(
//    activity: Activity,
//    value: String,
//    onTextChange: String.() -> Unit = {},
//    onSearch: String.() -> Unit = {},
//) {
//    Row(
//        Modifier
//            .fillMaxWidth()
//            .height(44.dp)
//            .padding(end = 20.dp, start = 10.dp)
//            .background(Color.White),
//        verticalAlignment = Alignment.CenterVertically
//    ) {
//        IconButton(
//            onClick = {
//                activity.onBackPressed()
//            }
//        ) {
//            Icon(Icons.Outlined.ArrowBack, "back", tint = Color.Black)
//        }
//        CustomTextField(
//            hint = value,
//            modifier = Modifier
//                .fillMaxWidth()
//                .height(40f.dp)
//                .background(colorResource(id = R.color.colorF5F5F5), shape = RoundedCornerShape(8.dp))
//                .padding(start = 10.dp),
//            leadingIcon = {
//                ImageIcon(R.drawable.icon_search_black, 24.dp)
//            }, showCleanIcon = true,
//            onTextChange = onTextChange,
//            keyboardActions = {
//                onSearch.invoke(this)
//            },
//            keyboardOptions = KeyboardOptions(imeAction = ImeAction.Search)
//
//
//        )
//    }
//}
//
//
