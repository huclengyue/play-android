package com.apkdv.playandroid.entity


import android.os.Parcelable
import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Keep
@Parcelize
data class PublicNumberListItem(
    @SerializedName("courseId")
    var courseId: Int = 0,
    @SerializedName("id")
    var id: Int = 0,
    @SerializedName("name")
    var name: String = "",
    @SerializedName("order")
    var order: Int = 0,
    @SerializedName("parentChapterId")
    var parentChapterId: Int = 0,
    @SerializedName("userControlSetTop")
    var userControlSetTop: Boolean = false,
    @SerializedName("visible")
    var visible: Int = 0
) : Parcelable