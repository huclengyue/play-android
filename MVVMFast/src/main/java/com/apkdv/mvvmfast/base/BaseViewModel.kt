package com.apkdv.mvvmfast.base

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

open class BaseViewModel : ViewModel() {
    //job列表
    private val jobs: MutableList<Job> = mutableListOf()
    fun request(block: suspend CoroutineScope.() -> Unit) =
        viewModelScope.launch { block() }.apply { jobs.add(this) }

    override fun onCleared() {
        jobs.forEach { it.cancel() }
        super.onCleared()
    }

}