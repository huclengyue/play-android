package com.apkdv.mvvmfast.compose.statelayout

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier

@Composable
fun EmptyPage(onReload: () -> Unit = {}) {
    Box(modifier = Modifier
        .fillMaxSize()
        .clickable { onReload.invoke() }) {
        Text(
            text = "这里什么都没有",
            Modifier.align(Alignment.Center)
        )
    }
}