package com.apkdv.mvvmfast.compose.statelayout

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import com.apkdv.mvvmfast.entity.*

@Composable
fun <T> StateLayout(
    state: LoadState,
    modifier: Modifier = Modifier, onReload: () -> Unit = {},
    showLoading: Boolean = false,
    content: @Composable (T) -> Unit,
) {
    Box(modifier = modifier.fillMaxSize()) {
        when (state) {
            is LoadLoading -> {
                LoadingPage(Modifier.align(Alignment.Center))
//                onReload.invoke()
            }
            is LoadNano -> {
                Box(modifier = Modifier.fillMaxSize())
                onReload.invoke()
            }
            is LoadError -> {
                ErrorPage(onReload = onReload)
            }
            is LoadSuccess<*> -> {
                content(state.data as T)
                if (showLoading) {
                    Box(
                        modifier = Modifier
                            .fillMaxSize()
                            .background(Color(0x80000000))
                            .clickable { }
                    ) {
                        CircularProgressIndicator(
                            modifier = Modifier.align(Alignment.Center),
                            color = Color.White
                        )
                    }
                }
            }
            is LoadEmpty -> {
                EmptyPage(onReload)
            }
        }
    }


}