package com.apkdv.mvvmfast.entity;

/**
 * Created by lengyue on 2018/5/27.
 * Email : lengyue@apkdv.com
 * <p>
 * Description :无返回数据接口的解析对象
 */
final public class EmptyData {
}
