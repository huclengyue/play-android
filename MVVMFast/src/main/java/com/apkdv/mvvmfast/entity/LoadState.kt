package com.apkdv.mvvmfast.entity

import com.apkdv.mvvmfast.network.exception.ApiException

sealed class LoadState
object LoadLoading : LoadState()
object LoadEmpty : LoadState()
object LoadNano : LoadState()
data class LoadSuccess<T>(val data: T) : LoadState()
data class LoadError(val e: ApiException) : LoadState()