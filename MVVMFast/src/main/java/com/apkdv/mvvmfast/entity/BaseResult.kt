package com.apkdv.mvvmfast.entity

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

@Keep
data class BaseResult<T>(
    @SerializedName("errorCode") private val code: Long,
    @SerializedName("errorMsg") private val msg: String,
    @SerializedName("data") private val data: T
) {
    fun getCode(): Long {
        return code
    }

    fun getMsg(): String {
        return msg
    }

    fun getData(): T {
        return data
    }

    fun isSuccess(): Boolean {
        return code.toInt() == 0
    }

}