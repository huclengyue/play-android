package com.apkdv.mvvmfast.network


import android.app.Application
import com.apkdv.mvvmfast.network.log.HttpLoggingInterceptor

import okhttp3.OkHttpClient
import rxhttp.RxHttpPlugins
import rxhttp.wrapper.ssl.HttpsUtils
import java.util.concurrent.TimeUnit
import javax.net.ssl.SSLSession


class RxHttpManager(val context: Application) {

    init {
        val sslParams = HttpsUtils.getSslSocketFactory()
        val client: OkHttpClient = OkHttpClient.Builder()
            .connectTimeout(15, TimeUnit.SECONDS)
            .readTimeout(15, TimeUnit.SECONDS)
            .writeTimeout(15, TimeUnit.SECONDS)
            .sslSocketFactory(sslParams.sSLSocketFactory, sslParams.trustManager) //添加信任证书
            .hostnameVerifier { hostname: String?, session: SSLSession? -> true } //忽略host验证
            //            .followRedirects(false)  //禁制OkHttp的重定向操作，我们自己处理重定向
            .addInterceptor(
                HttpLoggingInterceptor()
                    .setLevel(HttpLoggingInterceptor.Level.BODY)
            )
//                        .addInterceptor(new TokenInterceptor())
            .build()
        //设置缓存策略，非必须
//        File cacheFile = new File(context.getExternalCacheDir(), "RxHttpCache");
        //RxHttp初始化，非必须
        RxHttpPlugins.init(client) //自定义OkHttpClient对象
            .setDebug(false) //是否开启调试模式，开启后，logcat过滤RxHttp，即可看到整个请求流程日志
            //            .setCache(cacheFile, 1000 * 100, CacheMode.REQUEST_NETWORK_FAILED_READ_CACHE)
            //            .setExcludeCacheKeys("time")               //设置一些key，不参与cacheKey的组拼
            //            .setResultDecoder(s -> s)                  //设置数据解密/解码器，非必须
            .setConverter(GsonConverter.create())  //设置全局的转换器，非必须

    }
}