package com.apkdv.mvvmfast.network

import androidx.compose.material.Text
import androidx.compose.runtime.Composable

class api {
    @Composable
    fun Greeting(name: String) {
        Text(text = "Hello $name!")
    }
}