package com.apkdv.mvvmfast.ext

import com.apkdv.mvvmfast.event.SingleLiveEvent

class UIChange {
     val showDialog by lazy { SingleLiveEvent<String>() }
     val dismissDialog by lazy { SingleLiveEvent<Void>() }
     val toastEvent by lazy { SingleLiveEvent<String>() }

    // 显示数据
    // 显示数据
    val showEmpty by lazy { SingleLiveEvent<Void>() }
     val showError by lazy { SingleLiveEvent<Void>() }
     val showErrorMsg by lazy { SingleLiveEvent<String>() }
     val showLoading by lazy { SingleLiveEvent<Void>() }
     val showNoNet by lazy { SingleLiveEvent<Void>() }
     val showContent by lazy { SingleLiveEvent<Void>() }
}