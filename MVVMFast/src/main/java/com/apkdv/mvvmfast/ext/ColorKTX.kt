package com.jmbon.group.ket

import android.content.Context
import android.os.Build
import androidx.annotation.ColorRes
import androidx.annotation.DoNotInline
import androidx.annotation.RequiresApi
import androidx.compose.ui.graphics.Color


fun Context.colorResource(@ColorRes id: Int): Color {
    return if (Build.VERSION.SDK_INT >= 23) {
        ColorResourceHelper.getColor(this, id)
    } else {
        @Suppress("DEPRECATION")
        Color(this.resources.getColor(id))
    }
}

@RequiresApi(23)
private object ColorResourceHelper {
    @DoNotInline
    fun getColor(context: Context, @ColorRes id: Int): Color {
        return Color(context.resources.getColor(id, context.theme))
    }
}