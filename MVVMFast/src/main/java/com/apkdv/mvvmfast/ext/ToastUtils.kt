package com.apkdv.mvvmfast.ext

import android.widget.Toast
import androidx.annotation.StringRes
import com.blankj.utilcode.util.StringUtils
import com.blankj.utilcode.util.Utils

object ToastUtils {
    fun show(msg: String) {
        Toast.makeText(Utils.getApp(), msg, Toast.LENGTH_SHORT).show()
    }

    fun show(@StringRes res: Int) {
        Toast.makeText(Utils.getApp(), StringUtils.getString(res), Toast.LENGTH_SHORT).show()
    }
}