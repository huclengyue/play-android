package com.apkdv.mvvmfast.ext

import android.text.TextUtils



fun Int.showToast() {
//    ToastUtils2.getDefaultMaker().setGravity(Gravity.CENTER,0,0).show(this)
    ToastUtils.show(this)
}

fun CharSequence?.showToast() {
    this?.let { ToastUtils.show(it.toString()) }
}

private fun show(showData: CharSequence) {
    if (TextUtils.isEmpty(showData)) {
        return
    }
}
