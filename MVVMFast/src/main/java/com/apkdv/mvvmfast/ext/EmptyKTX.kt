package com.apkdv.mvvmfast.ext

fun CharSequence?.isNotNullEmpty(): Boolean {
    return !this.isNullOrEmpty() && this != "null"
}

fun <T> Collection<T>?.isNotNullEmpty(): Boolean {
    return !this.isNullOrEmpty()
}
